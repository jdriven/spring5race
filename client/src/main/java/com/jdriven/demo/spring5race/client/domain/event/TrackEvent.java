package com.jdriven.demo.spring5race.client.domain.event;

import com.jdriven.demo.spring5race.client.domain.Sector;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
public class TrackEvent {

    private final UUID id;
    private final UUID raceEventId;
    private final UUID trackId;
    private final UUID carId;
    private final Sector sector;
    private final Instant timestamp;
}
