package com.jdriven.demo.spring5race.client;

import com.jdriven.demo.spring5race.client.domain.event.TrackEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * This TrackEventCollector connects to the server and subscribes to all it's race events.
 */
@Service
public class TrackEventCollector {

    private static final String BASE_URL = "http://localhost:8080";
    private static final String ALL_RACES_URL_FORMAT = "/race/allevents.stream";
    private static final String TRACK_EVENTS_URL_FORMAT = "/race/%s/allevents.stream";

    private Flux<TrackEvent> trackEvents;

    private TrackEventCollector(WebClient.Builder webClientBuilder) {

        /**
         * TODO: client #1 Get the all TrackEvents for all RaceEvents
         * Step 1 Build the webclient with the Base URL
         * Step 2 Retrieve ALL_RACES as RaceEvent
         * Step 3 Map each race to get all TrackEvent for each race, so that you end up with 1 single Flux of TrackEvent.
         *
         * Tip: Check the Flux share function
         * Note: Nothing happens until someone subscribes.
         *
         * Next: Let's do something with the TrackEvent flux, implement TrackInfoSampler
         */

        // dummy implementation
        this.trackEvents = Flux.empty();

    }

    /**
     * Get Flux of TrackEvents from the race.
     *
     * @return Flux of TrackEvents.
     */
    public Flux<TrackEvent> getTrackEvents() {
        return trackEvents;
    }
}
