package com.jdriven.demo.spring5race.client.domain;

/**
 * Total track length 8,0 m
 * 1:32 (auto)
 * 1:24 (baan)
 * 24 baanstukken
 * 10 bochten
 * 1e deel 4 bochten, 2e deel 6 bochten
 * Binnen en buiten baan
 * http://www.carrera-toys.com/en/products/digital-132/sets/pure-speed-1321/#30191
 */
public enum Sector {
    START_FINISH,
    SECTOR_1,
    SECTOR_2
}
