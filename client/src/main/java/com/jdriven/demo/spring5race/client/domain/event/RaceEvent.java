package com.jdriven.demo.spring5race.client.domain.event;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
public class RaceEvent {
    private final UUID id;
    private final UUID trackId;
    private final Instant startTime;
}
