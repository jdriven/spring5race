package com.jdriven.demo.spring5race.client;

import com.jdriven.demo.spring5race.client.domain.event.TrackEvent;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class TrackInfoSampler implements ApplicationRunner, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(TrackInfoSampler.class);

    private final TrackEventCollector trackEventCollector;

    private final Map<String, CarAverage> carAverages = Collections.synchronizedMap(new HashMap<>());

    private Disposable subscription;

    @Override
    public void run(ApplicationArguments args) {
        logger.info("Starting subscription with trackEventCollector service");
        // after subscribe the stream starts (otherwise nothing happens)
        this.subscription = sampleCarAverage().subscribe();
    }

    @Override
    public void destroy() {
        this.subscription.dispose();
    }

    private Flux<CarAverage> sampleCarAverage() {
        Flux<TrackEvent> trackEvents = trackEventCollector.getTrackEvents();

        /**
         * TODO: client #2 Calculate the average time per round in a race for a car
         * A race never ends, so you'll have an infinite stream of TrackEvents.
         * Tip: use window and groupby function and store averages in parameter carAverages
         * For now log the averages or do something else with it :-)
         */

        // Dummy implementation
        return trackEvents.map(te -> {// Remove the map and implement the TODO's
                CarAverage ca = new CarAverage();
                ca.setCarId(te.getCarId());
                return ca;
            }
        ).doOnNext(
            ca -> logger.info("Car[{}] average: {} round : {}", ca.getCarId(), ca.getAverageDuration(), ca.getRound())
        );

    }

    @Data
    private static class CarAverage {
        private UUID carId;
        private long round = 0;
        private double average = 0.0;
        private long previous = 0;

        void add(long time) {
            round++;
            if (previous == 0.0) { // start
                previous = time;
            } else if (average == 0.0) { // 1e ronde
                average = time - previous;
                previous = time;
            } else {
                average = (average + (time - previous)) / 2;
                previous = time;
            }
        }

        Duration getAverageDuration() {
            return Duration.ofMillis(Double.valueOf(this.average).intValue());
        }
    }

}
