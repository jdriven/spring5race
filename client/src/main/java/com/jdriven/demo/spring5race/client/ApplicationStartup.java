package com.jdriven.demo.spring5race.client;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ApplicationStartup
    implements ApplicationListener<ApplicationReadyEvent> {

    private TrackEventCollector trackEventCollector;

    /**
     * Connect after the application is started
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        trackEventCollector.getTrackEvents().log().subscribe(trackEvent -> {
        });
    }

}