package com.jdriven.demo.spring5race;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring5RaceApplication {

    /*
    TODO -> Component scan indexer
    With the component scan indexer the beans are not scanned on runtime but generated on compile time.

    Add compile('org.springframework:spring-context-indexer') to the dependencies of spring5race/build.gradle
    After rebuild check the generated spring.components at spring5race/out/production/classes/META-INF/spring.components
    Check which beans are in there.

    Run the test Spring5raceApplicationTests#shouldHaveOneRaceSimulatorBean
    Run the application, does it still run? Is it faster? Probably not...
    When is this feature useful? On cloud providers with slow IO or there are more than 200 beans.
    More info at https://jira.spring.io/browse/SPR-11890?focusedCommentId=131391&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-131391
    */


    public static void main(String[] args) {
        SpringApplication.run(Spring5RaceApplication.class, args);
    }
}
