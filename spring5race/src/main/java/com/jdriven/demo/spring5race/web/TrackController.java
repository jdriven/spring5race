package com.jdriven.demo.spring5race.web;

import com.jdriven.demo.spring5race.domain.Track;
import com.jdriven.demo.spring5race.repository.TrackRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;
import java.util.UUID;

@Controller
@AllArgsConstructor
public class TrackController {

    private final TrackRepository trackRepository;

    @GetMapping(value = "/tracks/{trackId}")
    @ResponseBody
    public Optional<Track> tracks(@PathVariable String trackId) {
        return trackRepository.findById(UUID.fromString(trackId));
    }

    @GetMapping(value = "/tracks")
    @ResponseBody
    public Iterable<Track> tracks() {
        return trackRepository.findAll();
    }
}
