package com.jdriven.demo.spring5race.repository;

import com.jdriven.demo.spring5race.domain.Car;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CarRepository extends CrudRepository<Car, UUID> {
}
