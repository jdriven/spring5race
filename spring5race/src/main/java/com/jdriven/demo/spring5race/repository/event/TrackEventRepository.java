package com.jdriven.demo.spring5race.repository.event;

import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.UUID;

public interface TrackEventRepository extends ReactiveSortingRepository<TrackEvent, UUID> {

    Flux<TrackEvent> findByRaceEventId(UUID raceEventId);

    @Tailable
    Flux<TrackEvent> findWithTailableCursorByRaceEventId(UUID raceEventId);

    @Tailable
    Flux<TrackEvent> findWithTailableCursorByRaceEventIdAndTimestampAfter(UUID raceEventId, Instant timestamp);
}
