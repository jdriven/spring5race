package com.jdriven.demo.spring5race;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.support.GenericApplicationContext;

public class ProgrammaticBeanDefinitionInitializer implements ApplicationContextInitializer<GenericApplicationContext> {
    @Override
    public void initialize(GenericApplicationContext context) {

        /**
         * TODO register the RaceSimulator with the new functional bean registration
         * Step 1 : Remove @Component from the RaceSimulator class
         * Step 2 : enable this class in (remove the #) META-INF/spring.factories
         * Step 3 : register the bean programmatically
         *
         * https://static.javadoc.io/org.springframework/spring-context/5.0.0.RELEASE/org/springframework/context/support/GenericApplicationContext.html#registerBean-java.lang.String-java.lang.Class-java.util.function.Supplier-org.springframework.beans.factory.config.BeanDefinitionCustomizer...-
         * Check also what you can do with the last arguments (BeanDefinitionCustomizer) ...
         * More info at https://spring.io/blog/2017/03/01/spring-tips-programmatic-bean-registration-in-spring-framework-5
         *
         * Advantages are : no classpath scanning for beans, just normal lambda's but a lot of wiring....
         */

    }
}
