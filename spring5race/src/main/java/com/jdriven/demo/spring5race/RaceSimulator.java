package com.jdriven.demo.spring5race;

import com.jdriven.demo.spring5race.domain.Car;
import com.jdriven.demo.spring5race.domain.Sector;
import com.jdriven.demo.spring5race.domain.Track;
import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import com.jdriven.demo.spring5race.repository.CarRepository;
import com.jdriven.demo.spring5race.repository.TrackRepository;
import com.jdriven.demo.spring5race.repository.event.RaceEventRepository;
import com.jdriven.demo.spring5race.repository.event.TrackEventRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Component
@AllArgsConstructor
public class RaceSimulator implements ApplicationRunner {

    private final CarRepository carRepository;
    private final TrackRepository trackRepository;
    private final RaceEventRepository raceEventRepository;
    private final TrackEventRepository trackEventRepository;

    @Override
    public void run(ApplicationArguments args) {
        System.out.println("Starting RaceSimulator");
        startRace();
    }

    public void startRace() {
        System.out.println("-- Start new race -- ");
        Track track = this.getTrack(DataInitializer.JDRIVEN_TRACK);

        /*
            TODO Prevent callback hell and blocking calls by chaining the functions below properly.

            The code below explicitly subscribes to every intermediate (non-blocking) method call, and calls the .block()
            method. Therefore, it's not very reactive. Rewrite the code so that it properly chains functional reactive
            method calls, without nesting.

            Tip #1: Look at 'flatMap' and 'flatMapMany' (and the difference between them)
            Tip #2: Don't forget to consume the reactive stream (note the '.subscribe(System.out::println)' below)
         */
        startRaceEvent(track)
            .map(raceEvent -> this.raceEventRepository.save(raceEvent).block())
            .subscribe(raceEvent -> this.trackEvents(raceEvent, carRepository.findAll())
                .subscribe(trackEvent -> this.trackEventRepository.save(trackEvent)
                    .subscribe(System.out::println))
            );
    }

    private Track getTrack(String trackName) {
        return this.trackRepository.findByName(trackName)
            .orElseThrow(() -> new IllegalStateException("Could not find track " + trackName));
    }

    private Mono<RaceEvent> startRaceEvent(Track track) {
        RaceEvent raceEvent = RaceEvent.builder()
            .id(UUID.randomUUID())
            .trackId(track.getId())
            .startTime(Instant.now())
            .build();

        return Mono.just(raceEvent);
    }

    private Flux<TrackEvent> trackEvents(RaceEvent raceEvent, Iterable<Car> cars) {
        int rounds = 10;
        return Flux.fromIterable(cars)
            .flatMap(car -> this.carTrackEvents(raceEvent, car, rounds));
    }

    public Flux<TrackEvent> carTrackEvents(RaceEvent raceEvent, Car car, int rounds) {
        Flux<TrackEvent> start = Flux.just(TrackEvent.builder()
            .id(UUID.randomUUID())
            .raceEventId(raceEvent.getId())
            .trackId(raceEvent.getTrackId())
            .carId(car.getId())
            .sector(Sector.START_FINISH)
            .timestamp(Instant.now())
            .build());

        return Flux.merge(start,
            Flux.range(1, rounds)
            .delayUntil(e -> Mono.delay(Duration.ofMillis(generateRoundTime())))
            .map(i -> TrackEvent.builder()
                .id(UUID.randomUUID())
                .raceEventId(raceEvent.getId())
                .trackId(raceEvent.getTrackId())
                .carId(car.getId())
                .sector(Sector.START_FINISH)
                .timestamp(Instant.now())
                .build())
        );
    }

    private Integer generateRoundTime() {
        // 4 -6 seconds
        Double random = ThreadLocalRandom.current().nextDouble(4, 6) * 1000;
        return random.intValue();
    }
}
