package com.jdriven.demo.spring5race.repository;

import com.jdriven.demo.spring5race.domain.Driver;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface DriverRepository extends CrudRepository<Driver, UUID> {
}
