package com.jdriven.demo.spring5race.domain.event;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@Document(collection = "raceevents")
@TypeAlias("raceevent")
public class RaceEvent {

    @Id
    private final UUID id;
    private final UUID trackId;
    private final Instant startTime;
}
