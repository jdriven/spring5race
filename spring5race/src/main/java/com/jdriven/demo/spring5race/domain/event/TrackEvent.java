package com.jdriven.demo.spring5race.domain.event;

import com.jdriven.demo.spring5race.domain.Sector;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@Document(collection = "trackevents")
@TypeAlias("trackevent")
public class TrackEvent {

    @Id
    private final UUID id;
    private final UUID raceEventId;
    private final UUID trackId;
    private final UUID carId;
    private final Sector sector;
    private final Instant timestamp;
}
