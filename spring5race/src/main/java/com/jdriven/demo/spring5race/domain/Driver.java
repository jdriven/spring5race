package com.jdriven.demo.spring5race.domain;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Builder
@RequiredArgsConstructor
@Document(collection = "drivers")
public class Driver {

    @Id
    private final UUID id;
    private final String name;
}
