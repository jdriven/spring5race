package com.jdriven.demo.spring5race.repository;

import com.jdriven.demo.spring5race.domain.Track;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TrackRepository extends CrudRepository<Track, UUID> {

    Optional<Track> findByName(String name);
}
