package com.jdriven.demo.spring5race;

import com.jdriven.demo.spring5race.domain.Car;
import com.jdriven.demo.spring5race.domain.Track;
import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import com.jdriven.demo.spring5race.repository.CarRepository;
import com.jdriven.demo.spring5race.repository.TrackRepository;
import com.mongodb.reactivestreams.client.MongoCollection;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * This class just fills the mongo database with some car and track data.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class DataInitializer implements ApplicationRunner {

    public static final String JDRIVEN_TRACK = "JDriven Track";

    private final ReactiveMongoTemplate mongoTemplate;
    private final TrackRepository trackRepository;
    private final CarRepository carRepository;

    @Override
    public void run(ApplicationArguments args) {
        this.mongoTemplate.collectionExists(RaceEvent.class)
            .filter(available -> !available)
            .flatMap(i -> this.createRaceEventCollection())
            .block();

        this.mongoTemplate.collectionExists(TrackEvent.class)
            .filter(available -> !available)
            .flatMap(i -> this.createTrackEventCollection())
            .block();

        if (!this.trackRepository.findByName(JDRIVEN_TRACK).isPresent()) {
            this.trackRepository.save(generateJDrivenTrack());
        }

        if (this.carRepository.count() == 0) {
            this.carRepository.save(this.generateCar("JDriven"));
            this.carRepository.save(this.generateCar("JCore"));
            this.carRepository.save(this.generateCar("BDR"));
        }
    }

    private Mono<MongoCollection<Document>> createRaceEventCollection() {
        CollectionOptions options = CollectionOptions.empty().size(104857600).capped();
        return mongoTemplate.createCollection(RaceEvent.class, options);
    }

    private Mono<MongoCollection<Document>> createTrackEventCollection() {
        CollectionOptions options = CollectionOptions.empty().size(104857600).capped();
        return mongoTemplate.createCollection(TrackEvent.class, options);
    }

    private Track generateJDrivenTrack() {
        return Track.builder()
            .id(UUID.randomUUID())
            .name(JDRIVEN_TRACK)
            .lengthInMeters(8.0)
            .scale(24.0)
            .build();
    }

    private Car generateCar(String name) {
        return Car.builder()
            .id(UUID.randomUUID())
            .name(name)
            .build();
    }
}
