package com.jdriven.demo.spring5race.repository.event;

import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.UUID;

public interface RaceEventRepository extends ReactiveSortingRepository<RaceEvent, UUID> {

    @Tailable
    Flux<RaceEvent> findWithTailableCursorBy();

    @Tailable
    Flux<RaceEvent> findWithTailableCursorByStartTimeAfter(Instant startTime);
}
