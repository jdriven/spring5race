package com.jdriven.demo.spring5race.web;

import com.jdriven.demo.spring5race.RaceSimulator;
import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import com.jdriven.demo.spring5race.repository.event.RaceEventRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@AllArgsConstructor
public class RaceEventController {

    private final RaceEventRepository raceEventRepository;
    private final RaceSimulator raceSimulator;

    @GetMapping(value = "/race/start")
    @ResponseBody
    public Mono<Void> startRace() {
        raceSimulator.startRace();
        return Mono.empty();
    }

    @GetMapping(value = "/race")
    @ResponseBody
    public Flux<RaceEvent> getAllRaceEvents() {
        System.out.println("getAllRaceEvents");
        return this.raceEventRepository.findAll(Sort.by(Sort.Order.desc("startTime")));
    }

    @GetMapping(value = "/race/allevents.stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<RaceEvent> getAllRaceEventsStream() {
        return this.raceEventRepository.findWithTailableCursorBy();
    }
}

