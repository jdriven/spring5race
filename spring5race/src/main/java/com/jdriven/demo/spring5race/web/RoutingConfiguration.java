package com.jdriven.demo.spring5race.web;

import com.jdriven.demo.spring5race.repository.event.TrackEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class RoutingConfiguration {

    @Bean
    RouterFunction<?> trackEventRoutes(TrackEventRepository trackEventRepository) {
        // TODO implement the routes for the TrackEventController
        // Step 1 : Implement the RouterFunctions
        // see https://docs.spring.io/spring/docs/5.0.0.RELEASE/spring-framework-reference/web-reactive.html#webflux-fn-router-functions
        // Tip use the RouterFunctions#nest path route and andRoute functions
        // Step 2: Disable the TrackEventController
        // Step 3: Test the solution (run Spring5RaceApplicationTests)
        //
        // https://docs.spring.io/spring/docs/5.0.0.RELEASE/spring-framework-reference/web-reactive.html#webflux-fn-handler-functions

        // dummy implementation:
        return RouterFunctions.route(path("/"),
            request -> ok().body(BodyInserters.fromObject("Implement routing configuration for TrackEventController")));

    }
}
