package com.jdriven.demo.spring5race.web;

import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import com.jdriven.demo.spring5race.repository.event.TrackEventRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.UUID;

@Controller
@AllArgsConstructor
public class TrackEventController {

    private final TrackEventRepository trackEventRepository;

    // TODO Rewrite the Controller as a functional controller in class RoutingConfiguration.
    // See the tests in Spring5RaceApplicationTests with the WebTestClient

    @GetMapping(value = "/race/{raceEventId}/allevents")
    @ResponseBody
    public Flux<TrackEvent> getAllTrackEvents(@PathVariable String raceEventId) {
        return this.trackEventRepository.findByRaceEventId(UUID.fromString(raceEventId));
    }

    @GetMapping(value = "/race/{raceEventId}/allevents.stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<TrackEvent> getAllTrackEventsStream(@PathVariable String raceEventId) {
        System.out.println("getAllTrackEventsStream connection");
        return this.trackEventRepository.findWithTailableCursorByRaceEventId(UUID.fromString(raceEventId));
    }

    @GetMapping(value = "/race/{raceEventId}/events.stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<TrackEvent> getTrackEventsStream(@PathVariable String raceEventId) {
        return trackEventRepository.findWithTailableCursorByRaceEventIdAndTimestampAfter(UUID.fromString(raceEventId), Instant.now());
    }
}
