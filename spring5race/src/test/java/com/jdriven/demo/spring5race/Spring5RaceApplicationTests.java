package com.jdriven.demo.spring5race;

import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@DataMongoTest // disabled: this will start the embedded mongo but excludes all components, see documentation
@AutoConfigureDataMongo // to start only the EmbeddedMongoAutoConfiguration
public class Spring5RaceApplicationTests {
    @Value("${local.server.port}")
    private int localServerPort;

    @Autowired
    private ApplicationContext context;

    @Test
    public void shouldHaveOneRaceSimulatorBean() {
        Map<String, RaceSimulator> raceSimulatorMap = context.getBeansOfType(RaceSimulator.class);

        assertThat(raceSimulatorMap.size(), is(1));
    }

    @Test
    public void raceStartAutomaticOnStartup() {
        WebTestClient client = WebTestClient.bindToServer().baseUrl("http://localhost:" + localServerPort).build();

        FluxExchangeResult<RaceEvent> result = client.get().uri("/race/allevents.stream")
            .accept(MediaType.TEXT_EVENT_STREAM)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.TEXT_EVENT_STREAM)
            .returnResult(RaceEvent.class);


        StepVerifier.create(result.getResponseBody())
            .consumeNextWith(raceEvent -> assertNotNull(raceEvent.getStartTime()))
            //.expectNextCount(1)
            .thenCancel()
            .verify(Duration.ofSeconds(10));
    }

    @Test
    public void testTrackEventsForNotExistingRaceEvent() {
        WebTestClient client = WebTestClient.bindToServer().baseUrl("http://localhost:" + localServerPort).build();
        String notExistingRaceEventId = "14d8a53f-6ea4-42b6-b3ae-360d4c5b3d8e";
        FluxExchangeResult<TrackEvent> result = client.get().uri(String.format("/race/%s/allevents", notExistingRaceEventId))
            .accept(MediaType.TEXT_EVENT_STREAM)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.TEXT_EVENT_STREAM)
            .returnResult(TrackEvent.class);


        StepVerifier.create(result.getResponseBody())
            .expectSubscription()
            .expectComplete()
            .verify(Duration.ofSeconds(20));
    }
}
