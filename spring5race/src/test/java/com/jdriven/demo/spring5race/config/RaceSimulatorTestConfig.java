package com.jdriven.demo.spring5race.config;

import com.jdriven.demo.spring5race.DataInitializer;
import com.jdriven.demo.spring5race.RaceSimulator;
import com.jdriven.demo.spring5race.domain.Track;
import com.jdriven.demo.spring5race.repository.CarRepository;
import com.jdriven.demo.spring5race.repository.TrackRepository;
import com.jdriven.demo.spring5race.repository.event.RaceEventRepository;
import com.jdriven.demo.spring5race.repository.event.TrackEventRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;

//@Configuration
public class RaceSimulatorTestConfig {
    @Bean
    RaceSimulator raceSimulator() {
        CarRepository carRepository = Mockito.mock(CarRepository.class);
        TrackRepository trackRepository = Mockito.mock(TrackRepository.class);
        RaceEventRepository raceEventRepository = Mockito.mock(RaceEventRepository.class);
        TrackEventRepository trackEventRepository = Mockito.mock(TrackEventRepository.class);
        when(trackRepository.findByName(DataInitializer.JDRIVEN_TRACK))
            .thenReturn(Optional.of(
                Track.builder()
                    .id(UUID.randomUUID())
                    .name(DataInitializer.JDRIVEN_TRACK)
                    .lengthInMeters(8.0)
                    .scale(24.0)
                    .build())
            );

        return new RaceSimulator(carRepository, trackRepository, raceEventRepository, trackEventRepository);
    }
}
