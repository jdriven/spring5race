package com.jdriven.demo.spring5race;

import com.jdriven.demo.spring5race.config.RaceSimulatorTestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

// import org.springframework.test.context.junit.jupiter.EnabledIf;


/**
 * TODO : implement the unit test carTrackEvents
 * Check the documentation:
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#integration-testing-annotations-junit-jupiter
 *
 * Step 1 : Remove @EnabledIf
 * Check the conditions that you could set on a class or method (Disabled, DisabledIf) annotations are new in spring 5
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#enabledif
 *
 * Step 2 : Add the test and autowire the RaceSimulator as method parameter with @Autowired for the test
 *
 * Step 2a: run the test : it would fail on ParameterResolutionException 'No ParameterResolver registered for parameter ....' will occur
 *              you need the SpringExtension
 * Step 3 : Add @ExtendWith(SpringExtension.class) to the class
 *
 * Tip check the build.gradle how to configure JUnit 4 and 5 (junit-vintage-engine)
 *
 * https://howtoprogram.xyz/2017/09/12/junit-5-spring-boot-example/
 * https://github.com/spring-projects/spring-framework/wiki/What's-New-in-Spring-Framework-5.x#testing-improvements
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#testcontext-junit-jupiter-extension
 * https://howtoprogram.xyz/2017/09/12/junit-5-spring-boot-example/
 *
 * https://github.com/spring-projects/spring-boot/issues/6402
 *
 * https://github.com/sbrannen/spring-test-junit5
 * https://github.com/sbrannen/junit5-demo/blob/master/src/test/java/extensions/CaptureSystemOutput.java
 *
 */

@ContextConfiguration(classes = {RaceSimulatorTestConfig.class})
@EnabledIf("false")
public class RaceSimulatorJUnit5Test {
    @Test
    public void dummy() {
        // implement the carTrackEvents test
    }

    @EnabledIf(expression = "#{systemProperties['os.name'].toLowerCase().contains('linux') " +
        "OR systemProperties['os.name'].toLowerCase().contains('windows') " +
        "OR systemProperties['os.name'].toLowerCase().contains('mac')}", reason = "Enabled on Linux, Windows and Mac OS")
    @Test
    public void osTest() {
        assumeTrue(() -> System.getProperties().getProperty("os.name").toLowerCase().contains("solaris"));
        fail("This should never fail because it is an assumption!");
    }
}
