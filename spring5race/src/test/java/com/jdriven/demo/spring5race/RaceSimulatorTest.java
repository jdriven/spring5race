package com.jdriven.demo.spring5race;

import com.jdriven.demo.spring5race.config.RaceSimulatorTestConfig;
import com.jdriven.demo.spring5race.domain.Car;
import com.jdriven.demo.spring5race.domain.event.RaceEvent;
import com.jdriven.demo.spring5race.domain.event.TrackEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.UUID;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RaceSimulatorTestConfig.class})
public class RaceSimulatorTest {
    @Autowired
    private RaceSimulator raceSimulator;

    @Test
    public void carTrackEvents() {
        RaceEvent raceEvent = RaceEvent.builder()
            .id(UUID.randomUUID())
            .trackId(UUID.randomUUID())
            .startTime(Instant.now())
            .build();

        Car car = Car.builder().id(UUID.randomUUID()).name("dummyCar").build();
        int nrOfRounds = 3;

        // SUT
        Flux<TrackEvent> trackEventFlux = raceSimulator.carTrackEvents(raceEvent, car, nrOfRounds);

        /**
         * TODO: Fix the test with a StepVerifier. Note: A round takes about 4 to 6 seconds.
         * We don't want to wait for 6 seconds * 3 rounds for the unit test to complete,
         * so we will have to figure out a way to simulate the time
         *
         * Don't forget to set a timeout when verifying! We don't want to wait infinite, specially when the test fails.
         *
         * https://projectreactor.io/docs/test/release/api/reactor/test/StepVerifier.html
         */
    }


}